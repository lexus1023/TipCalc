package uce.optativa.tipcalc;

import android.app.Application;

/**
 * Created by Alexis on 05/11/2016.
 */

public class TipCalcApp extends Application{
    private final static String ABOUT_URL="https://about.me/alexisbautista";

    public  String getAboutUrl(){
        return ABOUT_URL;
    }
}
