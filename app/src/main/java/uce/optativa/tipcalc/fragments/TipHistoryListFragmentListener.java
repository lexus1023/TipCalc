package uce.optativa.tipcalc.fragments;

import uce.optativa.tipcalc.models.TipRecord;

/**
 * Created by Alexis on 29/10/2016.
 */

public interface TipHistoryListFragmentListener {

    void addToList(TipRecord record);
    void clearList();

}
